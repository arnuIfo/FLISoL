# FLISOL 2022
18º Festival Latinoamericano de Instalación de Software Libre el sabado 23 de abril 2022. En Panamá se estara celebrando en diferente localidades. https://flisol.info/
#### Fecha:  23 de Abril del 2022
#### Lugar: Virtual
#### Horario: 9:00 am a 4:30 pm
### [*Registrate* ](https://www.floss-pa.net/events/98) es gratis


|       EXPOSITORES  |                           TEMA                 |    CATEGORÍA   | HORARIO  |
|--------------------|------------------------------------------------|----------------|----------|
|FLOSSPA Team        | Bienvenida                                      |                |09:00 - 09:05  |
|  | ¿Qué es el software libre y como contribuyó?      | General        |09:05 - 09:30  |
| [Arnulfo Reyes](https://twitter.com/arnuIfo_07)  | No le temas a la línea de comandos - Fedora Forever | Básico |09:30 - 10:30  |
| Danilo Dominguez  |   | Programacion |10:30 - 11:30     |
|  |   |   |11:30 - 12:30  |
| **Almuerzo**            | **Almuerzo**                                        |                |12:30 - 13:00 |
| Ismael Valderrama |   | Programacion  |01:00 - 02:00  |
| Jonathan Guaynora |   | General  |02:00 - 03:00  |
|  |   |   |03:00 - 03:30  |
|  |   |  Ciberseguridad |03:30 - 04:30  |

## Organizado por:
- ### [Fundacion Panameña Tecnologias Libre](https://floss-pa.net/), [Comunidad de Floss Panamá](https://floss-pa.net/) y Falculta de Ingenieria UIP
- ###### [Maryon Torres](https://twitter.com/maryitotr),[Adbel Martinez](https://twitter.com/abdelgmartinezl), [Ismael Valderrama](https://www.linkedin.com/in/ismael-valderrama-67933b17), [Luis Segundo](https://blackfile.dev), [Danilo](), [Alejandro Perez]()


## Patrocinadores
[<img src="https://portal.uip.edu.pa/resources/images/institutions/banner_uip.png" width="170">](https://www.uip.edu.pa) 
[<img src="https://pbs.twimg.com/profile_images/852597051808522240/5iJqsWQL_400x400.jpg" width="170">](https://floss-pa.net) 
[<img src="https://s3.us-east-1.amazonaws.com/cdn.soluprime.io/logo_h.png" width="170">](http://soluprime.io) 
[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Fedora_logo_and_wordmark.svg/800px-Fedora_logo_and_wordmark.svg.png?20170315143305" width="170">](https://getfedora.org) 
[<img src="https://cytlastechnology.com/wp-content/uploads/2021/02/Cytlas-2-Sin-fondo.png" alt="Cytlas Technology Labs" width="170">](https://cytlastechnology.com)


