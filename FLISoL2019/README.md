# FLISoL 2019
14º Festival Latinoamericano de Instalación de Software Libre el 27 de abril 2019. En Panamá se estara celebrando en diferente localidades. https://flisol.info/
#### Fecha:  27 de Abril del 2019
#### Lugar: Auditorio de la Universidad Interamericana de Panamá (UIP)
#### Horario: 9:00 am a 3:00 pm
### [*Registrate* ](https://floss-pa.net/events/68) es gratis


|       EXPOSITORES  |                           TEMA                 |    CATEGORÍA   | HORARIO  |
|--------------------|------------------------------------------------|----------------|----------|
|FLOSSPA Team        | Bienvenida                                      |                |09:00 - 09:05  |
| Alejandro Perez  | ¿Qué es el software libre y como contribuyó?      | General        |09:05 - 09:30  |
| Sheyla Leacock  | Introducción al escaneo de red y vulnerabilidades con nmap  | Seguridad     |09:30 - 10:00  |
| Frank Sanabria       | Infraestructura con OpenSource (Colombia)     | General  |10:00 - 10:30     |
| Cecilio Niño       | Big data / Ciencia de Datos Open Source | Gestión de tecnología          |10:30 - 11:00  |
| Dr. Eduardo French Prettel   | Gestionando Proyecto con ProjectLibre| General            |11:00 - 11:30  |
| Nelly Valdivieso | Colaboración Diseñador UX/UI | Diseño               |11:30 - 12:00  |
| Almuerzo            |Almuerzo                                        |                |12:00 - 12:30  |
| Ariel Vernaza | Arquitectura de Agentes Inteligentes con Plataformas de Software Libre | Inteligencia Artificial  |12:30 - 01:00  |
| Victor Tejada Yau|   Beneficios de la colaboración en el Open Source | General               |01:00 - 01:30  |
| Luis Mora  | Como hacer música con software libre mediante Ardour, LMMS, Audacity e Hydrogen    | General |01:30 - 02:00  |
| Alex Gutierrez | Construyendo Edificación con OpenBIM                |  Diseño              |02:00 - 02:30  |
| Mario Andrée Orrego Zárate |  Simulador de red GNS3 (Perú) | Redes |02:30 - 03:00  |

## Organizado por:
- ### [Comunidad de Floss Panamá](https://floss-pa.net/) y Falculta de Ingenieria UIP
- ###### [Nelly Valdivieso](), [Andres Abadias](https://twitter.com/David25LO), [Shelsy Chanis](https://twitter.com/shelsxacm), [Josue](), [Valeria Vivero](), [Juan]() y [Maryon Torres](https://twitter.com/maryitotr)

## Patrocinadores
[<img src="https://www.uip.edu.pa/wp-content/uploads/2017/08/Logo-UIP-jpg.jpg" width="170">](https://www.uip.edu.pa)
[<img src="https://floss-pa.net/assets/logo-2089c566b25a632fff8df0a89b3bd82812ce72c9f63f90e895544a502fcfb58e.png" width="170">](https://getfedora.org )
[<img src="https://getfedora.org/static/images/fedora-logotext.png" width="170">](https://getfedora.org )

